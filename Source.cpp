#include <iostream>
#include <iomanip>
#include<Windows.h>

const unsigned int x = 5;
using namespace std;
int ary[x][x];
int sum = 0;

int main()
{
    SYSTEMTIME st;
    GetLocalTime(&st);
    setlocale(0, "Russian");
    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < x; j++)
        {
            ary[i][j] = (i + j);
            cout << std::setw(2) << ary[i][j];
            if (st.wDay % x == i)
            {
              sum += (i + j);
            }
        }
        cout << endl;
    }
    cout << " Line amount = " << sum;
}
